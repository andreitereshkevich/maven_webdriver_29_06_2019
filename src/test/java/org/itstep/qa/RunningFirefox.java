package org.itstep.qa;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class RunningFirefox {
    public static void main (String [] args){
        System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver24.exe");
        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://www.tut.by/");
    }
}
