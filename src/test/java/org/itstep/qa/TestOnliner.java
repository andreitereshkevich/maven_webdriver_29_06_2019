package org.itstep.qa;
/** На странице регистрации сайте onliner написать следующие тесты
 1. Попытка регистрации с некорректным email (проверять текст
 ошибки
 2. Попытка регистрации с несовпадающими паролями
 3. Попытка регистрации с пустыми полями */
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestOnliner {
    @Test
    public void testRegistrationWithIncorrectEmail() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver75.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://profile.onliner.by/registration");
        String incorrectEmailValue = "1";
        WebElement element =
                driver.
                        findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[5]/div/div/div/div/input"));
        element.sendKeys(incorrectEmailValue);
        WebElement incorrectEmailErrorMessage =
                driver.
                        findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[5]/div/div/div[2]/div"));
        Assert.assertTrue(incorrectEmailErrorMessage.isDisplayed());
    }
    @Test
    public void testRegistrationWithDifferentPasswords() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver75.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://profile.onliner.by/registration");
        String controlValue = "1@mail.ru";
        WebElement element =
                driver.
                        findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[5]/div/div/div/div/input"));
        element.sendKeys(controlValue);
        String password1 = "12345678";
        String password2 = "01234567";
        WebElement passwordFirstInput =
                driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[6]/div/div/div/div/input"));
        passwordFirstInput.sendKeys(password1);
        WebElement passwordSecondInput =
                driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[8]/div/div/div/div/input"));
        passwordSecondInput.sendKeys(password2);
        WebElement differentPasswordsError =
                driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[8]/div/div/div[2]/div"));
        Assert.assertTrue(differentPasswordsError.isDisplayed());
    }
    @Test
    public void testRegistrationWithEmptyFields() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver75.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://profile.onliner.by/registration");
        WebElement submitButton =
                driver.
                        findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[9]/button"));
        submitButton.click();
        WebElement inputEmailErrorMessage =
                driver.
                        findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[5]/div/div/div[2]/div"));
        WebElement inputPasswordErrorMessage =
                driver.
                        findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[6]/div/div/div[2]/div"));
        WebElement repeatPasswordErrorMessage =
                driver.
                        findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[8]/div/div/div[2]/div"));

       Assert.assertTrue(inputEmailErrorMessage.isDisplayed());
       Assert.assertTrue(inputPasswordErrorMessage.isDisplayed());
       Assert.assertTrue(repeatPasswordErrorMessage.isDisplayed());
    }
}
