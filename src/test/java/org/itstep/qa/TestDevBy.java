package org.itstep.qa;
/**На странице регистрации сайте dev.by написать следующие тесты
 1. Попытка регистрации с некорректным email
 2. Попытка регистарции с пустыми паролем
 3. Проверка наличия на странице регистрации всех
 обязательных элементов*/
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestDevBy {
    @Test
    public void testIncorrectEmailDevBy() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver75.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://id.dev.by/@/welcome");
        WebElement inputEmailField =
                driver.
                        findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[1]/input"));
        inputEmailField.sendKeys("1mailru");
        WebElement passwordField =
                driver.
                        findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[2]/div/input"));
        passwordField.sendKeys("1");
        WebElement userNameField =
                driver.
                        findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[3]/input"));
        userNameField.sendKeys("1");
        WebElement phoneNumberField =
                driver.
                        findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[4]/div/input"));
        phoneNumberField.sendKeys("1");
        WebElement userAgreementCheckbox =
                driver.
                        findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/div[3]/label[1]/div"));
        userAgreementCheckbox.click();
        WebElement submitButton =
                driver.
                        findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/button/span"));
        submitButton.click();
        WebElement incorrectEmailErrorMessage =
                driver.
                        findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/span"));
        Assert.assertTrue(incorrectEmailErrorMessage.isDisplayed());
    }

    @Test
    public void testEmptyPasswordDevBy() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver75.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://id.dev.by/@/welcome");
        WebElement inputEmailField =
                driver.
                        findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[1]/input"));
        inputEmailField.sendKeys("1@mail.ru");
        WebElement userNameField =
                driver.
                        findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[3]/input"));
        userNameField.sendKeys("1");
        WebElement phoneNumberField =
                driver.
                        findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[4]/div/input"));
        phoneNumberField.sendKeys("1");
        WebElement userAgreementCheckbox =
                driver.
                        findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/div[3]/label[1]/div"));
        userAgreementCheckbox.click();
        WebElement submitButton =
                driver.
                        findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/button/span"));
        submitButton.click();
        WebElement noPasswordMessage =
                driver.
                        findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/span"));
        Assert.assertTrue(noPasswordMessage.isDisplayed());
    }
    @Test
    public void testChekingRequaredFieldsDevBy(){
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver75.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://id.dev.by/@/welcome");
        WebElement titlePage =
                driver.
                        findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/ul/a[2]"));
        WebElement inputEmailField =
                driver.
                        findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[1]/input"));
        WebElement passwordField =
                driver.
                        findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[2]/div/input"));
        WebElement phoneNumberField =
                driver.
                        findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[4]/div/input"));
        WebElement userAgreementCheckbox =
                driver.
                        findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/div[3]/label[1]/div"));
        WebElement submitButton =
                driver.
                        findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/button/span"));
        Assert.assertTrue(titlePage.isDisplayed());
        Assert.assertTrue(inputEmailField.isEnabled());
        Assert.assertTrue(passwordField.isEnabled());
        Assert.assertTrue(phoneNumberField.isEnabled());
        Assert.assertTrue(userAgreementCheckbox.isEnabled());
        Assert.assertTrue(submitButton.isDisplayed());
    }
}

